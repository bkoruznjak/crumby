package com.premiumbinary.crumby.model;

import java.util.UUID;

/**
 * Created by bkoruznjak on 06/01/2017.
 */

public class CrumbModel {
    private String crumbId;
    private String crumbUserId;
    private String crumbMessage;
    private int crumbUserMood;
    private double crumbLatitude;
    private double crumbLongitude;
    private long crumbDropTime;

    public CrumbModel() {
    }

    public CrumbModel(String crumbUserId, String crumbMessage, int crumbUserMood, double crumbLatitude, double crumbLongitude) {
        this.crumbId = UUID.randomUUID().toString();
        this.crumbUserId = crumbUserId;
        this.crumbMessage = crumbMessage;
        this.crumbUserMood = crumbUserMood;
        this.crumbLatitude = crumbLatitude;
        this.crumbLongitude = crumbLongitude;
        this.crumbDropTime = System.currentTimeMillis();
    }

    public String getCrumbId() {
        return crumbId;
    }

    public void setCrumbId(String crumbId) {
        this.crumbId = crumbId;
    }

    public String getCrumbUserId() {
        return crumbUserId;
    }

    public void setCrumbUserId(String crumbUserId) {
        this.crumbUserId = crumbUserId;
    }

    public String getCrumbMessage() {
        return crumbMessage;
    }

    public void setCrumbMessage(String crumbMessage) {
        this.crumbMessage = crumbMessage;
    }

    public int getCrumbUserMood() {
        return crumbUserMood;
    }

    public void setCrumbUserMood(int crumbUserMood) {
        this.crumbUserMood = crumbUserMood;
    }

    public double getCrumbLatitude() {
        return crumbLatitude;
    }

    public void setCrumbLatitude(double crumbLatitude) {
        this.crumbLatitude = crumbLatitude;
    }

    public double getCrumbLongitude() {
        return crumbLongitude;
    }

    public void setCrumbLongitude(double crumbLongitude) {
        this.crumbLongitude = crumbLongitude;
    }

    public long getCrumbDropTime() {
        return crumbDropTime;
    }

    public void setCrumbDropTime(long crumbDropTime) {
        this.crumbDropTime = crumbDropTime;
    }

}
