package com.premiumbinary.crumby.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import com.premiumbinary.crumby.R;
import com.premiumbinary.crumby.model.CrumbModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by bkoruznjak on 07/01/2017.
 */

public class CrumbListAdapter extends RecyclerView.Adapter<CrumbListAdapter.CrumbViewHolder> {

    private final String pattern = "dd-MM-yyyy";
    private ArrayList<CrumbModel> dataSet;
    private SimpleDateFormat simpleDateFormat;

    public CrumbListAdapter(ArrayList<CrumbModel> data) {
        this.dataSet = data;
        simpleDateFormat = new SimpleDateFormat(pattern);
    }

    @Override
    public CrumbListAdapter.CrumbViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.crumb_card_layout, parent, false);

        CrumbListAdapter.CrumbViewHolder myViewHolder = new CrumbListAdapter.CrumbViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final CrumbListAdapter.CrumbViewHolder holder, final int listPosition) {
        TextView textViewDate = holder.crumbDate;
        TextView textViewTitle = holder.crumbMood;
        TextView textViewContent = holder.crumbTextContent;

        String formattedDate = simpleDateFormat.format(new Date(dataSet.get(listPosition).getCrumbDropTime()));
        textViewDate.setText(formattedDate);
        textViewTitle.setText(Integer.toString(dataSet.get(listPosition).getCrumbUserMood()));
        textViewContent.setText(dataSet.get(listPosition).getCrumbMessage());
        setScaleAnimation(holder.itemView);
    }

    @Override
    public void onViewDetachedFromWindow(CrumbListAdapter.CrumbViewHolder holder) {
        holder.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000);
        view.startAnimation(anim);
    }

    private void setScaleAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(1000);
        view.startAnimation(anim);
    }

    public static class CrumbViewHolder extends RecyclerView.ViewHolder {

        TextView crumbDate;
        TextView crumbMood;
        TextView crumbTextContent;
        View mItemView;

        public CrumbViewHolder(View itemView) {
            super(itemView);
            this.mItemView = itemView;
            this.crumbDate = (TextView) itemView.findViewById(R.id.text_crumb_date);
            this.crumbMood = (TextView) itemView.findViewById(R.id.text_crumb_mood);
            this.crumbTextContent = (TextView) itemView.findViewById(R.id.text_crumb_content);
        }

        public void clearAnimation() {
            if (mItemView != null) {
                mItemView.clearAnimation();
            }
        }
    }
}