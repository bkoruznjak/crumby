package com.premiumbinary.crumby.constants;

/**
 * Created by bkoruznjak on 07/01/2017.
 */

public class IntentExtraIdConstants {

    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
}
