package com.premiumbinary.crumby.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.premiumbinary.crumby.R;
import com.premiumbinary.crumby.adapters.CrumbListAdapter;
import com.premiumbinary.crumby.constants.IntentExtraIdConstants;
import com.premiumbinary.crumby.databinding.ActivityMainBinding;
import com.premiumbinary.crumby.model.CrumbModel;
import com.premiumbinary.crumby.root.CrumbyApp;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private Location mLastLocation;
    private String mUserUUID = "";
    private double mLatitude = 0.0;
    private double mLongitude = 0.0;
    private ActivityMainBinding mBinding;
    private DatabaseReference mCrumbDatabaseReference;
    private Query mUserCrumbQuery;
    private Query mMapCrumbQuery;
    private ChildEventListener mChildEventListener;
    private ChildEventListener mMapChildEventListener;
    private RecyclerView.Adapter mCrumbListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.crumbRecyclerView.setHasFixedSize(true);
        mBinding.crumbRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.crumbRecyclerView.setItemAnimator(new DefaultItemAnimator());
        // specify an adapter (see also next example)
        mCrumbListAdapter = new CrumbListAdapter(CrumbyApp.getInstance().getUserCrumbList());
        mBinding.crumbRecyclerView.setAdapter(mCrumbListAdapter);
        mCrumbDatabaseReference = FirebaseDatabase.getInstance().getReference().child("crumbs");
        mChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                if (previousChildName == null) {
                    CrumbyApp.getInstance().getUserCrumbList().clear();
                }
                Log.d("bbb", "onChildAdded:" + dataSnapshot.getKey());
                CrumbModel tempCrumb = dataSnapshot.getValue(CrumbModel.class);
                if (tempCrumb != null) {
                    CrumbyApp.getInstance().getUserCrumbList().add(dataSnapshot.getValue(CrumbModel.class));
                    mCrumbListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d("bbb", "onChildChanged:" + dataSnapshot.getKey());
                mCrumbListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("bbb", "onChildRemoved:" + dataSnapshot.getKey());
                mCrumbListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d("bbb", "onChildMoved:" + dataSnapshot.getKey());
                mCrumbListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("bbb", "postComments:onCancelled", databaseError.toException());
            }
        };

        mMapChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (s == null) {
                    CrumbyApp.getInstance().getMapCrumbList().clear();
                }
                Log.d("bbb", "onMapAdded:" + dataSnapshot.getKey());
                CrumbModel tempCrumb = dataSnapshot.getValue(CrumbModel.class);
                if (tempCrumb != null) {
                    CrumbyApp.getInstance().getMapCrumbList().add(dataSnapshot.getValue(CrumbModel.class));
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mBinding.buttonSubmitCrumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(mBinding.textCrumbMessage.getText().toString())) {
                    Toast.makeText(MainActivity.this, "You have no crumb to drop", Toast.LENGTH_SHORT).show();
                } else {
                    submitCrumb();
                }
            }
        });

        mBinding.fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

                } else {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                            mGoogleApiClient);
                    if (mLastLocation != null) {
                        mLatitude = mLastLocation.getLatitude();
                        mLongitude = mLastLocation.getLongitude();
                    }

                    Intent mapIntent = new Intent(MainActivity.this, MapActivity.class);
                    mapIntent.putExtra(IntentExtraIdConstants.LATITUDE, mLatitude);
                    mapIntent.putExtra(IntentExtraIdConstants.LONGITUDE, mLongitude);
                    startActivity(mapIntent);
                }
            }
        });

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        if (mUser != null) {
            mUserUUID = mUser.getUid();
            mUserCrumbQuery = mCrumbDatabaseReference.orderByChild("crumbUserId").equalTo(mUserUUID);
            mMapCrumbQuery = mCrumbDatabaseReference.orderByChild("crumbLatitude");
        }
    }

    void submitCrumb() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);

        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                mLatitude = mLastLocation.getLatitude();
                mLongitude = mLastLocation.getLongitude();
            }

            int userMoodValue = Integer.parseInt(getResources().getStringArray(R.array.mood_value_array)[mBinding.spinner.getSelectedItemPosition()]);
            CrumbModel crumb = new CrumbModel(mUserUUID, mBinding.textCrumbMessage.getText().toString(), userMoodValue, mLatitude, mLongitude);
            mCrumbDatabaseReference.child(crumb.getCrumbId()).setValue(crumb);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        mUserCrumbQuery.addChildEventListener(mChildEventListener);
        mMapCrumbQuery.addChildEventListener(mMapChildEventListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mUserCrumbQuery.removeEventListener(mChildEventListener);
        mMapCrumbQuery.removeEventListener(mMapChildEventListener);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("bbb", "connected to google api client");

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("bbb", "connection to google api client suspended i:" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("bbb", "connection to google api client failed with result:" + connectionResult);
    }
}
