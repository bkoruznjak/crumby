package com.premiumbinary.crumby.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.premiumbinary.crumby.R;
import com.premiumbinary.crumby.constants.IntentExtraIdConstants;
import com.premiumbinary.crumby.model.CrumbModel;
import com.premiumbinary.crumby.root.CrumbyApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final float DEFAULT_ZOOM_LEVEL = 16.0f;
    private final String mDatePattern = "dd-MM-yyyy";
    private float mCoordinateOffset = 0.0001f;
    private boolean isReducingOffset = false;
    private GoogleMap mMap;
    private ArrayList<CrumbModel> mCrumbList;
    private SimpleDateFormat simpleDateFormat;
    private double mLatitude = 0.0;
    private double mLongitude = 0.0;
    private Bitmap resizedMapIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mLatitude = getIntent().getDoubleExtra(IntentExtraIdConstants.LATITUDE, 0.0);
        mLongitude = getIntent().getDoubleExtra(IntentExtraIdConstants.LONGITUDE, 0.0);
        simpleDateFormat = new SimpleDateFormat(mDatePattern);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mCrumbList = CrumbyApp.getInstance().getMapCrumbList();
        resizedMapIcon = resizeMapIcons("crumb_light", 64, 64);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // add all crumbs
        for (CrumbModel crumb : mCrumbList) {
            //bit of offset for map icons with same location
            if (mCoordinateOffset <= 0.0001f) {
                isReducingOffset = false;
            } else if (mCoordinateOffset >= 0.0003f) {
                isReducingOffset = true;
            }

            if (isReducingOffset) {
                mCoordinateOffset -= 0.00001f;
                Log.d("bbb", "oduzimam:" + mCoordinateOffset);
            } else if (!isReducingOffset) {
                mCoordinateOffset += 0.00001f;
                Log.d("bbb", "dodajem:" + mCoordinateOffset);
            }

            LatLng crumbLocation = new LatLng(crumb.getCrumbLatitude() - mCoordinateOffset, crumb.getCrumbLongitude() + mCoordinateOffset);
            String formattedDate = simpleDateFormat.format(new Date(crumb.getCrumbDropTime()));
            Marker crumbMarker = mMap.addMarker(new MarkerOptions()
                    .position(crumbLocation)
                    .title(formattedDate)
                    .snippet(crumb.getCrumbMessage())
                    .icon(BitmapDescriptorFactory.fromBitmap(resizedMapIcon)));
        }
        //user marker
        LatLng userLocation = new LatLng(mLatitude, mLongitude);
        mMap.addMarker(new MarkerOptions().position(userLocation).title("Your location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, DEFAULT_ZOOM_LEVEL));


    }

    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }
}
