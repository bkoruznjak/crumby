package com.premiumbinary.crumby.root;

import android.app.Application;
import android.content.Context;

import com.premiumbinary.crumby.model.CrumbModel;
import com.squareup.leakcanary.LeakCanary;

import java.util.ArrayList;

/**
 * Created by bkoruznjak on 06/01/2017.
 */

public class CrumbyApp extends Application {

    private static CrumbyApp instance;
    private ArrayList<CrumbModel> mUserCrumbList;
    private ArrayList<CrumbModel> mMapCrumbList;

    public static CrumbyApp getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mUserCrumbList = new ArrayList<>();
        mMapCrumbList = new ArrayList<>();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }

    public ArrayList<CrumbModel> getUserCrumbList() {
        return mUserCrumbList;
    }

    public void setUserCrumbList(ArrayList<CrumbModel> mCrumbList) {
        this.mUserCrumbList = mCrumbList;
    }

    public ArrayList<CrumbModel> getMapCrumbList() {
        return mMapCrumbList;
    }

    public void setMapCrumbList(ArrayList<CrumbModel> mMapCrumbList) {
        this.mMapCrumbList = mMapCrumbList;
    }
}
